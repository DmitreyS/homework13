import { useState, useEffect } from "react";
import "./App.css";
import ProductList from "./Components/ProductList/ProductList";
import FetchProducts from "./Components/FetchProducts/FetchProducts";

function App() {
 
  const [tovari, setTovari] = useState([]);

  // Поисковая строка
  const [searchValue, setSearchValue] = useState("");


  const [foundedProducts, setFoundedProducts] = useState([]);

  useEffect(() => {
    setFoundedProducts(tovari);
  }, [tovari]);

  
  const titles = [];
  
  if (tovari.length > 0) {
    for (let i = 0; i < tovari.length; i++) {
      titles.push({ title: tovari[i].title.toLowerCase(), id: tovari[i].id });
    }
  }

  const foundSearchIds = [];
  if (searchValue.length >= 3) {
    titles.forEach((item) => {
      if (item.title.includes(searchValue.toLowerCase())) {
        foundSearchIds.push(item.id);
      }
    });
  }

  let tempFoundProducts = [];
  tovari.forEach((item) => {
    if (foundSearchIds.includes(item.id)) {
      tempFoundProducts.push(item);
    }
  });

  useEffect(() => {
    if (searchValue.length >= 3) {
      setFoundedProducts([...tempFoundProducts]);
    } else {
      setFoundedProducts(tovari);
    }
  }, [searchValue]);

  // ----- Select -----

  const categories = ["Все товары"];
 
  if (tovari.length > 0) {
    for (let i = 0; i < tovari.length; i++) {
      if (!categories.includes(tovari[i].category)) {
        categories.push(tovari[i].category);
      }
    }
  }

  const [selectValue, setSelectValue] = useState("Все товары");

  let tempFoundProductsFromSelect = [];
  tovari.forEach((item) => {
    if (item.category === selectValue) {
      tempFoundProductsFromSelect.push(item);
    }
  });

  useEffect(() => {
    if (selectValue !== "Все товары") {
      setFoundedProducts([...tempFoundProductsFromSelect]);
    } else {
      setFoundedProducts(tovari);
    }
  }, [selectValue]);

  return (
    <div className="App">
      <ProductList
        tovari={foundedProducts}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        categories={categories}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

      <FetchProducts setTovari={setTovari} />
    </div>
  );
}

export default App;
