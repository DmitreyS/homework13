import "./Product.scss";
import Rating from '@mui/material/Rating';

function Product({ product }) {
  // console.log(product);

  return <div className="product">
    {product !== undefined ?
      <>
        <h3>{product.title}</h3>
        <p>{product.description}</p>
        <img src={product.image} className="product__image" alt={product.title} />
        <p><b>{product.price} руб.</b></p>
        <p>Категория товара: <b>{product.category}</b></p>
        <p>Рейтинг: <b>{product.rating.rate} stars</b> - На основании {product.rating.count} оценок</p>
        <Rating name="read-only" value={product.rating.rate} precision={0.1} size="large" readOnly />
      </>
      : <p>Пустая карточка товара</p>
    }
    </div>
}

export default Product;